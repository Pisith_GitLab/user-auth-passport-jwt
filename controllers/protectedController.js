const protectedContent = async (req, res) => {
  return res.status(200).json({message: 'This is protected contents'});
}

module.exports = {
  protectedContent
}