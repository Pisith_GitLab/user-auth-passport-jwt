const User = require('../models/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

// Register a new user
const register = async (req, res) => {
  try {
    const { username, email, password } = req.body;
    // Generate a salt and hash the password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);
    // Create a new user in the database
    const user = await User.create({ username, email, password: hashedPassword });
    res.json({ message: 'User registered successfully', user });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Failed to register user' });
  }
};

// Login an existing user and generate a JWT token
const login = async (req, res) => {
  try {
    const { email, password } = req.body;
    // Find the user in the database
    const user = await User.findOne({ email });
    // If user not found, return error
    if (!user) {
      return res.status(404).json({ error: 'User not found' });
    }
    // Compare the provided password with the stored hashed password
    const isPasswordValid = await bcrypt.compare(password, user.password);
    // If password is invalid, return error
    if (!isPasswordValid) {
      return res.status(401).json({ error: 'Invalid password' });
    }
    // Generate a JWT token
    const token = jwt.sign({ sub: user._id }, process.env.JWT_SECRET_KEY, { expiresIn:'1h' }); 
    // Return the token to the client
    res.json({ message: 'Login successful', token });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Failed to login' });
  }
};

module.exports = {
  login,
  register
}