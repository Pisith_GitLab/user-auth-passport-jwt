const express = require('express')
const bodyParser = require('body-parser');
require('dotenv').config();
const connectDB = require('./dataSources/db');
const passport = require('passport');
const authRoute = require('./routes/authRoute');
const protectedRoute = require('./routes/protectedRoute');

const app = express();
const PORT = process.env.PORT || 3000;

// Initialize Passport and Passport JWT strategy
app.use(passport.initialize());
require('./configs/passport')(passport);

// middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// DB Connection
connectDB();

app.get('/', (req, res) => {
  res.status(200).json({message: 'Welcome to User-Auth-Passport Application'});
})

app.use('/api/v1/auth', authRoute);

// protected routes : routes
app.use('/api/v1/pro', protectedRoute);

// protected routes : app.js
app.get('/pro', async (req, res) => {
  // Access protected route only if user is authenticated
  passport.authenticate('jwt', { session: false }, (err, user) => {
    if (err) {
      // Handle any errors that occur during authentication
      console.error(err);
      return res.status(500).json({ error: 'Failed to authenticate' });
    }
    if (user) {
      // User is authenticated, access protected route
      return res.json({ message: 'Access granted to protected route', user });
    } else {
      // User is not authenticated, return error
      return res.status(401).json({ error: 'Unauthorized' });
    }
  })(req, res);
})

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
})
