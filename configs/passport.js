const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('../models/User');
require('dotenv').config();

// JWT options
const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.JWT_SECRET_KEY // Replace with your JWT secret key
};

module.exports = (passport) => {
  passport.use(
    new JwtStrategy(jwtOptions, async (jwtPayload, done) => {
      try {
        // Find the user by ID from JWT payload
        const user = await User.findById(jwtPayload.sub);
        if (user) {
          // If user is found, return user object
          return done(null, user);
        } else {
          // If user is not found, return false
          return done(null, false);
        }
      } catch (error) {
        // Handle any errors that occur during authentication
        console.error(error);
        return done(error, false);
      }
    })
  );
}