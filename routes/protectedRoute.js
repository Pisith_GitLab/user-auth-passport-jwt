const express = require('express')
const router = express.Router();
const protectedController = require('../controllers/protectedController');
const passport = require('passport');

// Passport JWT authentication middleware
router.use(passport.authenticate('jwt', { session: false }));

router.get('/content', protectedController.protectedContent);

module.exports = router;